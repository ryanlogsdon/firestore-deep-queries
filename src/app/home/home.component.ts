import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    user: Observable<any>;
    addresses: Observable<any>;

    constructor(private firestore: AngularFirestore) {
        this.user = null;
    }

    ngOnInit(): void {
        this.user = this.firestore.collection('users').doc('ryan@test.com').valueChanges();
        
        // this.addresses = this.firestore.collection('users').doc('ryan@test.com').collection('addresses').doc('mailingAddresses').valueChanges();
        
        this.addresses = this.firestore.doc('/users/ryan@test.com/addresses/mailingAddresses').valueChanges();
        
        this.addresses.subscribe(res => console.log('addresses', res));                 // print the object when the observable changes
    
    }

    delete(): void {
        this.firestore.collection('users').doc('ryan@test.com').delete().then(() => {
            console.log('top-level doc deleted');
        }).catch((error) => {
            console.error('error deleting doc', error);
        });
    }

}
